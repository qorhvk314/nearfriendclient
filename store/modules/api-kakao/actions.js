import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_SEARCH_ADDRESS]: (store, payload) => {
        let paramArray = [] // 파라미터 모을 배열
        // 만약에 페이로드(받은값) 안에 searchAddress(검색주소) 칸의 값이 한글자라도 존재한다면 위에서 만든 파라미터 모을 배열에 추가한다.
        if (payload.params.searchAddress.length >= 1) paramArray.push(`searchAddress=${payload.params.searchAddress}`)
        let paramText = paramArray.join('&') // 파라미터가 모아진 배열의 요소들을 싹 펼쳐서 사이사이에 & 단어를 넣어준다.

        return axios.get(apiUrls.DO_SEARCH_ADDRESS + '?' + paramText) // axios 전화기를 통해 완성된 주소로 전화를 걸고 응답값을 넘겨준다.
    },
}
